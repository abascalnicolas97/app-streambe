
import { NavigationContainer } from "@react-navigation/native"
import { MyTabs } from "./src/Navegacion/BottomTabNavigation"
import { StackPrincipal } from './src/Navegacion/stackDeNavegacion';
import { Provider } from 'react-redux'
import { store } from "./src/Store/store";

const App = () => {
  return (
    <NavigationContainer>
      <Provider store={store}>
      <StackPrincipal/>
      </Provider>
    </NavigationContainer>
  )
}

export default App    