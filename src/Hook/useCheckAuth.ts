import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../interfaces/interfaces';
import { useLayoutEffect,useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setContactos, setUser } from '../Store/Slice/usuario';
import { getContactos } from '../services/services';

export const useCheckAuth = () =>{
    const  usuario = useSelector( (state:RootState) => state.user.user );
    const [logeado,setLogeado]=useState(false)
    const dispatch = useDispatch();
    const getContactosUsuario = async () => {
        const {users} = await getContactos();
        dispatch(setContactos(users));
      };
    const init = async()=>{
        const userJson = await AsyncStorage.getItem('user')
        const user = userJson?JSON.parse(userJson):null
        if ( !user ){ 
            dispatch( setUser(null) );
        }
        else{ 
            dispatch( setUser(user) );
            setLogeado(true)
            getContactosUsuario()
        }
    }
    useLayoutEffect(() => {
        init()
      }, []);
    return logeado;
}