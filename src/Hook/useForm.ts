import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { setContactos, setUser } from '../Store/Slice/usuario';
import { getContactos } from '../services/services';
import AsyncStorage from '@react-native-async-storage/async-storage';
interface Object {
    [key: string]: string;
  }
export const useForm = <T extends Object>( initState: T,navigation:any ) => {
    const dispatch = useDispatch()
    const [state, setState] = useState( initState );
    const [errors,setErrors] = useState<{ [key: string]:string }>({});

    const onChange = ( value: string, field: keyof T ) => {
        setState({
            ...state,
            [field]: value
        });
    }

    const validation = () => {
      let newErrors: { [key: string]: string } = {}; 
    
     
      for (const formField of Object.keys(state)) {
        if (state[formField].length === 0) {
          newErrors[formField] = "Este campo es obligatorio";
        } else {
       
          if (newErrors[formField]) {
            delete newErrors[formField];
          }
        }
      }
    
      setErrors(newErrors);
      return Object.keys(newErrors).length === 0;
    };
    
      const getContactosUsuario = async () => {
        const {users} = await getContactos();
        dispatch(setContactos(users));
      };
    
    const login = async()=>{
        const isValid=validation()
        
        if( isValid){
          getContactosUsuario()
          await AsyncStorage.setItem('user',JSON.stringify({usuario:state.email,password:state.password}));
            dispatch(setUser({usuario:state.email,password:state.password}))
            navigation('home')
        }
    }
    const setFormValue = ( form: T ) =>{
        setState( form );
    }
   
    
    return {
        ...state,
        form: state,
        onChange,
        setFormValue,
        login,
        errors
    }

}