import React from 'react'
import { TouchableOpacity,View,StyleProp,ViewStyle,Text, TextStyle} from 'react-native'

interface Props {
    styleContaines: StyleProp<ViewStyle>;
    styTexto:StyleProp<TextStyle>;
    onPress:()=>void;
    Texto:string
}

const Button = ({styleContaines,styTexto,onPress,Texto}:Props) => {
  return (
    <TouchableOpacity onPress={onPress} style={styleContaines}>
        <Text style={styTexto}>
            {Texto}
        </Text>
    </TouchableOpacity>
  )
}

export default Button
