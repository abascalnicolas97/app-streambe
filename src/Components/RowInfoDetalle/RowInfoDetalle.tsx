import React from 'react'
import { StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

interface Props{
    icon:string
    tittle:string;
    data:string
    style?: StyleProp<ViewStyle>;
}

const RowInfoDetalle = ({icon,tittle,data,style}:Props) => {
  return (
    <View style={[styleRow.container,style]}>
    <View style={{flexDirection:'row',alignItems:'center'}}>
    <Icon name={icon}  size={25} color="#000000" />
    <Text style={styleRow.titulo}>{tittle}</Text>
    </View>
    <Text style={{fontSize:16}}>{data}</Text>

</View>
  )
}

const styleRow=StyleSheet.create({
    container:{
        paddingHorizontal:10,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        height:60,
        borderTopWidth:2,
        borderBottomWidth:2,
        borderColor:'#CDCDCD'
    },
    titulo:{
        fontSize:16,
        marginLeft:10
        ,fontWeight:'bold'}
  })
export default RowInfoDetalle
