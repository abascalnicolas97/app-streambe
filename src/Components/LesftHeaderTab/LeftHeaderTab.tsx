import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { Text, View } from 'react-native';

interface Props {
  name: string;
}

const LeftHeaderTab = ({ name }: Props) => {
  const [datos, setDatos] = useState<{ iconName: string; texto: string }>({
    iconName: 'home-outline',
    texto: '',
  });

  useEffect(() => {
    switch (name) {
      case 'Home':
        setDatos({ iconName: 'home-outline', texto: 'Tu lista de contacto' });
        break;
      case 'Muestras':
        setDatos({ iconName: 'medkit-outline', texto: name });
        break;
      case 'Vademecum':
        setDatos({ iconName: 'book-outline', texto: name });
        break;
      default:
        setDatos({ iconName: '', texto: '' });
    }
  }, []);

  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
      }}
    >
      <Icon name={datos.iconName} size={30} color="#fff" />
      <Text style={{ color: '#fff', marginLeft: 10, fontSize: 15 }}>
        {datos.texto}
      </Text>
    </View>
  );
};

export default LeftHeaderTab;
