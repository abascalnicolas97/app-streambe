import React from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native'
import { style } from '../../Theme/RowcontactosTheme'
import Icon from 'react-native-vector-icons/Ionicons';

interface Props {
    onPress:()=>void;
    photo:string | null;
    name:string;
    surname:string;
}
const RowContactos = ({onPress,photo,name,surname}:Props) => {
  return (
    <View style={style.container}>
        <View style={{flexDirection:'row',alignItems:'center'}}>
        <Image
          source={{
            uri: photo || 'https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif'
          }}
          style={ style.imagen }
        />
        <Text style={style.nombre}>
           {`${name} ${surname}`}
        </Text>
        </View>
        <TouchableOpacity style={style.containerChevron} onPress={onPress}>
        <Icon name={'chevron-forward-outline'} size={40} color={'#EC6463'} />
        </TouchableOpacity>
    </View>
  )
}

export default RowContactos
