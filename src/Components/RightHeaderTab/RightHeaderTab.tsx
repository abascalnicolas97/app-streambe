import {Text, View } from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import { notification } from "../../Theme/notificacion";

const RightHeaderTab = () => {
  return (
    <View style={notification.container}>
        <View>
        <View style={notification.notification}>
          <Text style={{fontSize:10,color:'white',fontWeight:'bold'}}>5</Text>
        </View>
      <View style={notification.circle}>
        <Icon name="notifications-outline" size={30} color="#EC6463" />
      </View>
      </View>
    </View>
  );
}


export default RightHeaderTab;

