import { StyleProp, StyleSheet, TouchableWithoutFeedback, View, ViewStyle,Linking  } from "react-native"
import Icon from 'react-native-vector-icons/Ionicons'


interface Props{
    style?: StyleProp<ViewStyle>;
    icon:string
    url:string
}

const IconOpcionDetalle = ({style,icon,url}:Props) => {

  return (
    <TouchableWithoutFeedback  onPress={()=>Linking.openURL(url)}>
        <View style={[styleOpcion.container,style]}>
            <Icon name={icon}  size={25} color="#fff" />
        </View>
    </TouchableWithoutFeedback>
  )
}
const styleOpcion =StyleSheet.create({
    container:{
        height:50,
        width:50,
        borderRadius:25,
        backgroundColor:'#EC6463',
        justifyContent:'center',
        alignItems:'center'
    }
})
export default IconOpcionDetalle
