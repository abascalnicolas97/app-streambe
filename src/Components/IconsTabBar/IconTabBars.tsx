
import React from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const IconTabBars = (name:string,focused:boolean) => {
  let iconName = '';
  switch (name) {
    case 'Home':
      iconName = 'home-outline';
      break;
    case 'Muestras':
      iconName = 'medkit-outline';
      break;
    case 'Vademecum':
      iconName = 'book-outline';
      break;
  }
  return (
    <View style={{ paddingVertical: 20, flex:1, alignItems: 'center',width:80,backgroundColor:`${focused?'rgba(241,239,239,0.2)':'transparent'}`,borderRadius:40}}>
        <Icon name={iconName} size={30} color={'#ffffff'} />
        <Text style={{ color: 'white',zIndex:2 }}>
        {name}
        </Text>

    </View>
  );
}



export default IconTabBars;

