import { createStackNavigator } from "@react-navigation/stack";
import Login from "../Screen/Login";
import { MyTabs } from "./BottomTabNavigation";
import DetalleContacto from "../Screen/DetalleContacto";
import { Contacto } from "../interfaces/interfaces";
import { useCheckAuth } from "../Hook/useCheckAuth";


export type StackPrincipalParams={
    login:undefined,
    home:undefined,
    Contacto:{contacto:Contacto}

}
const Stack =createStackNavigator<StackPrincipalParams> ();


export const StackPrincipal=()=>{

    const state =useCheckAuth();
    
    return(<Stack.Navigator screenOptions={{cardStyle:{
        backgroundColor:'white'
    }}}>
       
       {!state && 
        <Stack.Screen name="login" options={{ headerShown: false }} component={Login}/>
        }
        <Stack.Screen name="home" options={{ headerShown: false }} component={MyTabs}/>
        <Stack.Screen 
            name="Contacto"

            options={{headerStyle:{backgroundColor:'#EC6463',borderBottomWidth:0}
            ,headerTitleStyle:{color:'white'},headerTintColor:'white',}}
            component={DetalleContacto}/>
          
        
    </Stack.Navigator>)
}