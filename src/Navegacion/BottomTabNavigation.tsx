import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../Screen/Home';
import LeftHeaderTab from '../Components/LesftHeaderTab/LeftHeaderTab';
import RightHeaderTab from '../Components/RightHeaderTab/RightHeaderTab';
import Muestras from '../Screen/Muestras';
import Vademecum from '../Screen/Vademecum';
import IconTabBars from '../Components/IconsTabBar/IconTabBars';


const Tab = createBottomTabNavigator();

export function MyTabs() {
  return (
    <Tab.Navigator screenOptions={({route})=>({
        tabBarIcon:({focused})=>{
           return IconTabBars(route.name,focused)
        },
        headerStyle: {
            backgroundColor: '#EC6463', 
          },
          headerTitle:undefined, 
          title:'',

          tabBarStyle:{
              backgroundColor:'#EC6463',
              height:80,
          },
          tabBarLabelStyle:{
            height:0
          },
          headerLeft:() => <LeftHeaderTab name={route.name} />,
    })} initialRouteName='Home' >
      <Tab.Screen name="Muestras" component={Muestras} />
      <Tab.Screen options={{
          
          headerRight:()=> <RightHeaderTab/>,
         
          }} name="Home" component={Home} />
      <Tab.Screen name="Vademecum" component={Vademecum} />
    </Tab.Navigator>
  );
}