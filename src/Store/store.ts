import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./Slice/usuario";

export const store = configureStore({
    reducer:{
            user: userSlice.reducer
    }
}) 