import { createSlice } from "@reduxjs/toolkit";
import { Usuario } from '../../interfaces/interfaces';

const  userState:Usuario = {
    user:null,
    contactos:[]
}

const userSlice = createSlice({
    name:'user',
    initialState:userState,
    reducers:{
        setUser: (state,action) =>{
            state.user = action.payload
        },
        logOut: (state)=>{
            state.user=null
        },
        setContactos:(state,action)=>{
            state.contactos=action.payload
        },
        eliminarContacto:(state,action)=>{
            state.contactos=state.contactos.filter(contacto =>contacto.contactId != action.payload)
        }
    }
}) 

export const {setUser,logOut,setContactos,eliminarContacto}= userSlice.actions
export default userSlice