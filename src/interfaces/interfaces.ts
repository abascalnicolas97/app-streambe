
export interface Contacto {
    contactId:number;
    name:string;
    surnames:string;
    birthDate:string;
    gender:string;
    photo: string | null;
    phone:string;
    profesion:string;
    email:string

}

export interface Usuario{
    user:{usuario:string,password:string} | null,
    contactos:Contacto[]
} 

export interface responseContactos{
    users:Contacto[]
}


export type RootState = {
  user: Usuario;

};