import {Image, StyleSheet} from 'react-native';
import {Text, View} from 'react-native';
import IconOpcionDetalle from '../Components/IconOpcionDetalle/IconOpcionDetalle';
import RowInfoDetalle from '../Components/RowInfoDetalle/RowInfoDetalle';
import Button from '../Components/Button/Button';
import {StackScreenProps} from '@react-navigation/stack';
import {StackPrincipalParams} from '../Navegacion/stackDeNavegacion';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { useDispatch } from 'react-redux';
import { styleDetalle } from '../Theme/DetalleTheme';
import { eliminarContacto } from '../Store/Slice/usuario';

interface Props extends StackScreenProps<StackPrincipalParams, 'Contacto'> {}
const DetalleContacto = ({navigation, route}: Props) => {
  const dispatch= useDispatch()
  const {contacto} = route.params;

  const eliminar= ()=>{
    dispatch(eliminarContacto(contacto.contactId))
    navigation.goBack()
  }

  return (
    <View>
      <View style={{height: hp(20)}}>
        <View
          style={styleDetalle.conatinerHeader}></View>
        <View
          style={styleDetalle.circuloBlanco}>
          <Image
            source={{
              uri: `${
                contacto.photo
                  ? contacto.photo
                  : 'https://medgoldresources.com/wp-content/uploads/2018/02/avatar-placeholder.gif'
              }`,
            }}
            style={styleDetalle.foto}
          />
        </View>
      </View>
      <View style={{alignItems: 'center',marginTop:10}}>
        <Text
          style={{
            fontSize: 20,
            fontWeight: 'bold',
             textAlign:'center'
          }}>{`${contacto.name} ${contacto.surnames}`}</Text>
      </View>
      <View
        style={styleDetalle.containerIconDetalle}>
        <IconOpcionDetalle icon="call-outline" url={`tel:${contacto.phone}`} />
        <IconOpcionDetalle
          icon="mail-outline"
          style={{marginHorizontal: 15}}
          url={`mailto:${contacto.email}`}
        />
        <IconOpcionDetalle icon="logo-whatsapp" url={`whatsapp://send?${contacto.phone}`} />
      </View>
      <View style={{marginTop: 50}}>
        <RowInfoDetalle
          icon={'calendar-clear-outline'}
          tittle={'Fecha de nacimiento'}
          data={contacto.birthDate}
        />
        <RowInfoDetalle
          icon={'male-female-outline'}
          style={{borderTopWidth: 0, borderBottomWidth: 0}}
          tittle={'Género'}
          data={contacto.gender}
        />
        <RowInfoDetalle
          icon={'briefcase-sharp'}
          tittle={'Profesión'}
          data={contacto.profesion}
        />
      </View>
      <Button
        styleContaines={styleDetalle.button}
        styTexto={styleDetalle.textoBoton}
        Texto={'Eliminar de mi lista'}
        onPress={() =>eliminar()}
      />
    </View>
  );
};

export default DetalleContacto;
