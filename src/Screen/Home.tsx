import {View, FlatList} from 'react-native';
import RowContactos from '../Components/RowContactos/RowContactos';
import {StackScreenProps} from '@react-navigation/stack';
import {useSelector} from 'react-redux';
import {RootState} from '../interfaces/interfaces';

interface Props extends StackScreenProps<any, any> {}
const Home = ({navigation}: Props) => {
  const contacts = useSelector((state: RootState) => state.user.contactos);
  
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF', paddingTop: 40}}>
      <FlatList
        data={contacts}
        extraData={contacts}
        keyExtractor={item => item.surnames}
        renderItem={({item}) => (
          <RowContactos
            photo={item.photo}
            name={item.name}
            surname={item.surnames}
            onPress={() => {
              navigation.navigate('Contacto', {contacto:item});
            }}
          />
        )}
      />
    </View>
  );
};

export default Home;
