import React from 'react'
import { Text, View,TextInput, Touchable, TouchableOpacity} from 'react-native'
import { useForm } from '../Hook/useForm';
import { loginStyles } from '../Theme/loginTheme';
import { StackScreenProps } from '@react-navigation/stack';
import Button from '../Components/Button/Button';



interface Props extends StackScreenProps<any, any> {}

const Login = ({navigation}:Props) => {
    const { email, password,errors ,onChange,login } = useForm({
        email: '',
        password: '' 
     },navigation.navigate);
     
  return (
<View style={loginStyles.container}>
    <Text style={loginStyles.titulo}>
        Log in
    </Text>
    <TextInput placeholder='Email' value={email} onChangeText={ (value) => onChange(value, 'email') }  style={[loginStyles.inputField, errors['email'] ? loginStyles.error : null]} />
    <TextInput secureTextEntry={true} value={password} onChangeText={ (value) => onChange(value, 'password') } placeholder='Password'  style={[loginStyles.inputField, errors['password'] ? loginStyles.error : null]}/>
    
    <Button styleContaines={loginStyles.button} onPress={login} styTexto={{ color:'white',fontSize:15}} Texto='LOG IN'/>
</View>
  )
}

export default Login
