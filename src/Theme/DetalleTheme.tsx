import { StyleSheet } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
const anchoEnPixelesContainer = wp(35);
const alturaEnPixelesContainer  = hp(88);
const minSizeContainer = Math.min(anchoEnPixelesContainer, alturaEnPixelesContainer);
const anchoEnPixeles = wp(80);
const alturaEnPixeles  = hp(80);
const minSize = Math.min(anchoEnPixeles, alturaEnPixeles);



export const styleDetalle = StyleSheet.create({
    conatinerHeader:{
        height: '60%',
        backgroundColor: '#EC6463',
        borderBottomLeftRadius: 60,
        borderBottomRightRadius: 60,
        alignItems: 'center',
    },
    circuloBlanco:{
        height: '88%',
        width: '35%',
        backgroundColor: 'white',
        borderRadius: minSizeContainer / 2,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: hp(3),
        alignSelf: 'center',
    },
    foto:{
        height: '80%', 
        width: '80%', 
        borderRadius: minSize/2
    },
    containerIconDetalle:{
        flexDirection: 'row', 
        justifyContent: 'center', 
        marginTop: 10
    },
    button:{
        marginTop: 50,
        height: 50,
        backgroundColor: '#EC6463',
        width: '90%',
        alignSelf: 'center',
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textoBoton:{
        color: 'white', 
        fontSize: 20, 
        fontWeight: 'bold'
    }
})