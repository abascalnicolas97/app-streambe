import { StyleSheet } from "react-native";

export const notification = StyleSheet.create({
    container: {
      paddingHorizontal: 15,
      height:40,
      width: '100%',
      alignItems: 'flex-end', 
    },
    circle: {
      width: 40,
      height: 40,
      borderRadius: 20,
      backgroundColor: 'white',
      borderWidth: 1,
      borderColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
    },
    notification:{
        justifyContent:'center',
        alignItems:'center',
      width:15,
      height:15,
      backgroundColor:'#18A3F8',
      borderRadius:7.5,
      position:'absolute',
      right:-5,
      zIndex:2
    }
  });
  