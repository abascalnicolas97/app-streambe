import { StyleSheet } from "react-native";

export const style = StyleSheet.create({
    container:{
        borderBottomWidth:2,
        borderBottomColor:'#CDCDCD',
        height:100,
        paddingHorizontal:20,
        display:'flex',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    imagen:{
        width:70,
        height:70,
        borderRadius:35
    },
    nombre:{
        fontSize:16
        ,marginLeft:10,
        fontWeight:'bold'
    },
    containerChevron:{
        justifyContent:'center',
        height:70
    }

})