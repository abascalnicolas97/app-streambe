import { StyleSheet } from "react-native";

export const loginStyles = StyleSheet.create({
    inputField:{
        borderWidth:3,
        paddingHorizontal:15,
        fontSize:20,
        marginTop:20,
    },
    error:{
        borderWidth:3,
        paddingHorizontal:15,
        fontSize:20,
        marginTop:20,
        borderColor:'red'
    },
    container:{

        flex:1,
        display:'flex',
        paddingTop:'10%',
        paddingHorizontal:'5%'
    },
    button:{
        width:'100%',
        height:50,
        backgroundColor:'black',
        marginTop:20,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5
    },
    titulo:{
        fontSize:50,
        color:'black'
    }
})