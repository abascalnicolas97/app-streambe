import axios from "axios"
import {responseContactos } from "../interfaces/interfaces"

export const getContactos= async ():Promise<responseContactos>=>{
    const datos= (await axios.get('https://www.mockachino.com/06c67c77-18c4-45/users')).data
    return datos
}